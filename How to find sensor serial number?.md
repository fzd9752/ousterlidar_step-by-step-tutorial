## How to find open sensor home page without serial number?

- For Linux
- For Ouster Studio


### For Linux

1. Power on sensor and connected the ethernet cable to your compurter.
2. In `Terminal` run `easy-setup-set_Ethernet_interface.sh` script or below commands (check our [software user guide - 3.1 Network Configuration - Running A Local DHCP Server](https://data.ouster.io/downloads/software-user-guide-v1.13.0.pdf) for detail instruction):
	
	```
		# example code
	sudo ip -4 addr flush dev enp0s31f6 # this enp0s31f6 should be your ethernet
	sudo ip addr add 10.5.5.1/24 dev enp0s31f6
	sudo ip link set enp0s31f6 up
	sudo dnsmasq -C /dev/null -kd -F 10.5.5.50,10.5.5.100 -i enp0s31f6 --bind-dynamic

	```
3. You'll get below image if your DHCP set up properly and it will show the IP for your sensor. In this case, the sensor IP is 10.5.5.68

![](./imgs/DHCP_sensor_ip.png)
4. Visit **Sensor Home Page** by input your sensor IP at your browser, you could find many useful info through this page

![](./imgs/Sensor_Home_Page_En_Guide.PNG)

### For Ouster Studio
If you use our Ouster Studio software, you could directly check your lidar serial number or IP, and use this IP to access your sensor home page.

![](./imgs/Ouster_studio_stream_En_Guide.png)

