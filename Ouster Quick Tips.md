## Ouster Quick Tips

### Ouster Studio

1. Improve the success probability for connecting LiDAR 
	- Save configure file for each time 
	- Chosse 1024x10 method

2. There is NO **amibient** image in the Ouster Studio
![](2Dimage.png)

3. Record Point Cloud
	- Click red circle button to start recording a `.pcap` file
	- Select folder and save
	- Wait, Ouster Studio is recording
	- When you want to end, ***close the Sudio*** or start another record by click red cirle button (bug here, no command to close to record 😓)

4. Show data detail as below:
![](spreadsheet.png)
	- Slow, shut down all unnecessary app on your laptop

### LiDAR（if customers ask, no ask no mention)

1. Time Sync/授时 
	- 3 sources/三种来源, 10 nanosecond precision/精确度10纳秒
	- internal oscillator/内置震荡器
	- synchronize pulse/同步脉冲，NMEA(GPS)/GPRMC...
	- IEEE 1588 Precision Time Protocol(PTP) or other PTP/高精度时间同步协议

2. Data Transfer Speed - 64 beams example
	- UDP with gigabit
	- 512x10 - 32 Mbps
	- 512x20, 1024x10 - 64Mbps
	- 1024x20, 2048x10- 129Mbps

3. Data Latency/数据延迟 < 10ms
4. Data Per Point/数据包含每个点的:
	- Range, intensity, reflectivity, ambient, angle, time stamp/距离，反射强度，反射率，环境光强度， 角度，时间戳
	- Got cartisan coordinate(xyz) from polar coordinate(range,angle)/用**距离**和**角度**从极坐标计算笛卡尔坐标系位置信息

5. Ouster SDK
	- C++ driver/驱动
	- ROS driver/驱动