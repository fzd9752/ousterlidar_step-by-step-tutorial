## Ouster ROS Running without LiDAR
> Ubunut 18.04
> Ubuntu 16.04

### Pre-step (only need to be set up once)
##### Install ROS following ROS offical guide

- for Ubuntu 16.04, install [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- for Ubuntu 18.04, install [ROS Melodic](http://wiki.ros.org/melodic/Installation/Ubuntu)
- this tutorial use `Ubuntu 18.04` and `ROS Melodic` as example

##### Uncompress ouster official driver `ouster_example` folder to your local PC

- or download it with code `git clone https://github.com/ouster-lidar/ouster_example.git`
- in the following tutorial, the `ouster_example` folder directly stores at `home` repository, so the address for this forder is `/home/YourUserName/ouster_example` or `~/ouster_example`

##### Build Ouster example ros_node

1. Open a new terminal and typing`export CMAKE_PREFIX_PATH=~/ouster_example`
2. Load ROS script by typing `source /opt/ros/[kinetic_or_melodic]/setup.bash` 
3. Back `home` folder `cd ~`
4. Create ROS workspace folder and build ROS node in this folder by typing below command 
	
	```
	mkdir -p ros_ws/src && cd ros_ws && ln -s ~/ouster_example ./src/ && catkin_make -DCMAKE_BUILD_TYPE=Release`
	```
	
	![ouster_ros_build.png](./imgs/ouster_ros_build.png)
	
5. Successly build will show image as below:

	![ros_build_output.png](./imgs/ouster_ros_build_success.png)




### Run recorded `.bag` file without sensor connection

1. In the new terminal, load ROS and Ouster_ros scripts 
	
	```
	source /opt/ros/[kinetic_or_melodic]/setup.bash
	source ~/ros_ws/devel/setup.bash
	cd ~/ros_ws/src/ouster_example/ouster_ros
	```
	
2. Run ouster ros node 

	```
	roslaunch os1.launch replay:=true metadata:=<sensor_config_json_file_path> viz:=true image:=true
	```
![](./imgs/rosode_run.png)
	-  if `viz:=true`, ouster visualizer will pop out and show the recorded pointcloud when `.bag` running
	 ![](./imgs/rosnode_viz.png)
	- if `image:=true`, show the 2D images in `rviz`
	
3. Open a new termianl, run recorded `.bag` with below commands:
	
	```
	rosbag play --clock <bagfile_aboslute_path> -l
	```
![](./imgs/rosbag_play.png)
	- `-l` loop display

4. If you want to use `rviz` to display the recorded data, open a new terminal and run below commands:

	```
	cd ~/ros_ws/src/ouster_example/ouster_ros
	rviz -d viz.rviz
	```
![](./imgs/start_rviz.png)
	
5. Done!
![](./imgs/ros_final_output.png)