## ROS查看Ouster激光雷达录制数据
> 适用 Ubunut 18.04 及 Ubuntu 16.04

### 准备步骤 (一台PC只需配置一次)
##### 根据ROS官方安装指南正确安装ROS

- 如果你使用的是 Ubuntu 16.04, 请安装 [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- 如果你使用的是 18.04, 请安装 [ROS Melodic](http://wiki.ros.org/melodic/Installation/Ubuntu)
- 这个教程使用 `Ubuntu 18.04` 和 `ROS Melodic` 

##### 将官方开源文件 `ouster_example` 解压到你本地的机器上

- 或者用以下代码从GitHub上克隆 `git clone https://github.com/ouster-lidar/ouster_example.git`
- 以下教程中, `ouster_example` 文件夹默认存储与 `home` 目录下, 所以绝对地址是 `/home/YourUserName/ouster_example`，相对地址 `~/ouster_example`

##### 编译 Ouster 的 ROS Node

1. 打开一个新的命令行，输入`export CMAKE_PREFIX_PATH=~/ouster_example`
2. 加载相应版本的ROS配置文件，命令行输入 `source /opt/ros/[kinetic_or_melodic]/setup.bash` 
3. 返回 `home` 文件目录下，命令行输入 `cd ~`
4. 在命令行中输入下面的命令，创建ROS Node目录并编译：
	
	```
	mkdir -p ros_ws/src && cd ros_ws && ln -s ~/ouster_example ./src/ && catkin_make -DCMAKE_BUILD_TYPE=Release`
	```
	
	![ouster_ros_build.png](./imgs/ouster_ros_build.png)
	
5. 如下图显示则为编译成功:

	![ros_build_output.png](./imgs/ouster_ros_build_success.png)

> 安装过程中可能遇到的问题
> 
>  1. **fatal error: tclap/CmdLine.h: No such file or directory**
> > 命令行运行 `sudo apt install libtclap-dev`


### 无激光雷达运行录制的 `.bag` 文件

1. 在新的命令行中, 载入 ROS 和 Ouster_ros 的配置文件
	
	```
	source /opt/ros/[kinetic_or_melodic]/setup.bash
	source ~/ros_ws/devel/setup.bash
	cd ~/ros_ws/src/ouster_example/ouster_ros
	```
	
2. 运行Ouster的ROS node 

	```
	roslaunch os1.launch replay:=true metadata:=<录制用雷达配置文件的绝对地址> viz:=true image:=true
	```
![](./imgs/rosode_run.png)
	-  如果 `viz:=true`, 将会弹出 ouster visualizer 的窗口并在录制文件运行时显示点云和图像数据，如下图
	 ![](./imgs/rosnode_viz.png)
	- `image:=true`, 运行 `rviz` 时，显示2D图像
	
3. 打开另一个新的命令行, 用下面的命令运行录制的 `.bag` 文件:
	
	```
	rosbag play --clock <.bag文件的地址> -l
	```
![](./imgs/rosbag_play.png)
	- `-l` 循环播放

4. 如果想使用 `rviz` 显示录制的数据, 打开一个新的命令行，输入下面的命令:

	```
	cd ~/ros_ws/src/ouster_example/ouster_ros
	rviz -d viz.rviz
	```
![](./imgs/start_rviz.png)
	
5. 成功如下图!
6. 
![](./imgs/ros_final_output.png)